/*    This file is part of Murmelbox.

    Murmelbox is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Murmelbox is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Murmelbox.  If not, see <http://www.gnu.org/licenses/>.
*/
#include"saver.h"
#include"canvas.h"
#include"box.h"
#include"sphere.h"
#include<GL/gl.h>
#include<cstdlib>
#include<cmath>

using namespace std;

typedef struct Ball
{
	Sphere s;
	Vector3D vel;
	int dirty;
} _BALL;


float *sphere_verts;
float *sphere_normals;
static float yrot=0.0f;
static Ball* balls;

static float lightpos[]={0.0f,0.0f,0.0f,1.0f};
static float lightamb[]={0.3f,0.3f,0.3f,1.0f};
static float lightdif[]={0.7f,0.7f,0.7f,1.0f};
static float lightspc[]={0.9f,0.9f,0.9f,1.0f};

static float redmat[]={1.0f,0.0f,0.0f,1.0f};
static float normalmat[4]={1.0f,1.0f,1.0f,1.0f};

void GenCubeMaterial(float trans)
{
	normalmat[3]=trans;
}

void Update(Settings *set)
{
	yrot+=set->rotspeed/set->refreshRate;
	Vector3D* norms=(Vector3D*)face_norms;
	Vector3D* vecs=(Vector3D*)box_verts;
	for(int i=0;i<set->numBalls;i++)
	{
		balls[i].s.pos.x+=0.1f*set->ballspeed*balls[i].vel.x/set->refreshRate;
		balls[i].s.pos.y+=0.1f*set->ballspeed*balls[i].vel.y/set->refreshRate;
		balls[i].s.pos.z+=0.1f*set->ballspeed*balls[i].vel.z/set->refreshRate;

		if(DistPlanePoint(&vecs[0],&norms[0],&(balls[i].s.pos))<balls[i].s.r)
		{
			float d;
			Vector3D p={0.0f,0.0f,1.0f};
			p.z+=-norms[0].z*balls[i].s.r;
			d=IntersectPlaneRay(&p,&norms[0],&(balls[i].s.pos),&(balls[i].vel));
			balls[i].s.pos.x+=d*balls[i].vel.x;
			balls[i].s.pos.y+=d*balls[i].vel.y;
			balls[i].s.pos.z+=d*balls[i].vel.z;
			balls[i].vel.z*=-1;
		}
		if(DistPlanePoint(&vecs[4],&norms[1],&(balls[i].s.pos))<balls[i].s.r)
		{
			float d;
			Vector3D p={0.0f,0.0f,-1.0f};
			p.z+=-norms[1].z*balls[i].s.r;
			d=IntersectPlaneRay(&p,&norms[1],&(balls[i].s.pos),&(balls[i].vel));
			balls[i].s.pos.x+=d*balls[i].vel.x;
			balls[i].s.pos.y+=d*balls[i].vel.y;
			balls[i].s.pos.z+=d*balls[i].vel.z;
			balls[i].vel.z*=-1;
		}
		if(DistPlanePoint(&vecs[8],&norms[2],&(balls[i].s.pos))<balls[i].s.r)
		{
			float d;
			Vector3D p={1.0f,0.0f,0.0f};
			p.x+=-norms[2].x*balls[i].s.r;
			d=IntersectPlaneRay(&p,&norms[2],&(balls[i].s.pos),&(balls[i].vel));
			balls[i].s.pos.x+=d*balls[i].vel.x;
			balls[i].s.pos.y+=d*balls[i].vel.y;
			balls[i].s.pos.z+=d*balls[i].vel.z;
			balls[i].vel.x*=-1;
		}
		if(DistPlanePoint(&vecs[12],&norms[3],&(balls[i].s.pos))<balls[i].s.r)
		{
			float d;
			Vector3D p={-1.0f,0.0f,0.0f};
			p.x+=-norms[3].x*balls[i].s.r;
			d=IntersectPlaneRay(&p,&norms[3],&(balls[i].s.pos),&(balls[i].vel));
			balls[i].s.pos.x+=d*balls[i].vel.x;
			balls[i].s.pos.y+=d*balls[i].vel.y;
			balls[i].s.pos.z+=d*balls[i].vel.z;
			balls[i].vel.x*=-1;
		}
		if(DistPlanePoint(&vecs[16],&norms[4],&(balls[i].s.pos))<balls[i].s.r)
		{
			float d;
			Vector3D p={0.0f,1.0f,0.0f};
			p.y+=-norms[4].y*balls[i].s.r;
			d=IntersectPlaneRay(&p,&norms[4],&(balls[i].s.pos),&(balls[i].vel));
			balls[i].s.pos.x+=d*balls[i].vel.x;
			balls[i].s.pos.y+=d*balls[i].vel.y;
			balls[i].s.pos.z+=d*balls[i].vel.z;
			balls[i].vel.y*=-1;
		}
		if(DistPlanePoint(&vecs[20],&norms[5],&(balls[i].s.pos))<balls[i].s.r)
		{
			float d;
			Vector3D p={0.0f,-1.0f,0.0f};
			p.y+=-norms[5].y*balls[i].s.r;
			d=IntersectPlaneRay(&p,&norms[5],&(balls[i].s.pos),&(balls[i].vel));
			balls[i].s.pos.x+=d*balls[i].vel.x;
			balls[i].s.pos.y+=d*balls[i].vel.y;
			balls[i].s.pos.z+=d*balls[i].vel.z;
			balls[i].vel.y*=-1;
		}
	}
}

void SetupGeometry()
{
	GenSphere(0.1f,32,32,&sphere_verts,&sphere_normals);
}

void SetupBalls(int num)
{
	balls=(Ball*)malloc(sizeof(Ball)*num);
	for(int i=0;i<num;i++)
	{
		balls[i].s.r=0.1f;
		balls[i].s.pos.x=((rand()%5785)/5784.0f)-balls[i].s.r;
		if(rand()%32001>16000)
		{
			balls[i].s.pos.x*=-1;
		}
		balls[i].s.pos.y=((rand()%5785)/5784.0f)-balls[i].s.r;
		if(rand()%32001>16000)
		{
			balls[i].s.pos.y*=-1;
		}
		balls[i].s.pos.z=((rand()%5785)/5784.0f)-balls[i].s.r;
		if(rand()%32001>16000)
		{
			balls[i].s.pos.z*=-1;
		}
		balls[i].vel.x=(rand()%5785)/5784.0f;
		if(rand()%32001>16000)
		{
			balls[i].vel.x*=-1;
		}
		balls[i].vel.y=(rand()%5785)/5784.0f;
		if(rand()%32001>16000)
		{
			balls[i].vel.y*=-1;
		}
		balls[i].vel.z=(rand()%5785)/5784.0f;
		if(rand()%32001>16000)
		{
			balls[i].vel.z*=-1;
		}
		balls[i].vel=Normalize3D(&(balls[i].vel));
	}
}

void SetupLight()
{
	glLightfv(GL_LIGHT0,GL_POSITION,lightpos);
	glLightfv(GL_LIGHT0,GL_AMBIENT,lightamb);
	glLightfv(GL_LIGHT0,GL_SPECULAR,lightspc);
	glLightfv(GL_LIGHT0,GL_DIFFUSE,lightdif);
	glEnable(GL_LIGHT0);
}

void Draw(Settings* set)
{
	glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
	glLoadIdentity();
	glTranslatef(0.0f,0.0f,-(set->dist));
	glRotatef(yrot,0.0f,1.0f,0.0f);

	glDisable(GL_TEXTURE_2D);

	glDisableClientState(GL_TEXTURE_COORD_ARRAY);
	glMaterialfv(GL_FRONT_AND_BACK,GL_DIFFUSE,redmat);
	glVertexPointer(3,GL_FLOAT,0,sphere_verts);
	glNormalPointer(GL_FLOAT,0,sphere_normals);

	for(int i=0;i<set->numBalls;i++)
	{
		glPushMatrix();
		glTranslatef(balls[i].s.pos.x,balls[i].s.pos.y,balls[i].s.pos.z);
		glDrawArrays(GL_TRIANGLE_STRIP,0,32*32*2);
		glPopMatrix();
	}

	glEnableClientState(GL_TEXTURE_COORD_ARRAY);

	glEnable(GL_BLEND);
	glDepthMask(GL_FALSE);
	glMaterialfv(GL_FRONT_AND_BACK,GL_DIFFUSE,normalmat);

	glVertexPointer(3,GL_FLOAT,0,box_verts);
	glNormalPointer(GL_FLOAT,0,box_normals);
	glTexCoordPointer(2,GL_FLOAT,0,box_texcoords);

	glDrawArrays(GL_QUADS,4*4,2*4);

	glEnable(GL_TEXTURE_2D);

	glDrawArrays(GL_QUADS,0,4*4);

	glDepthMask(GL_TRUE);
	glDisable(GL_BLEND);

	SwapBuffer();
}

