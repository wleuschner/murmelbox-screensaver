/*    This file is part of Murmelbox.

    Murmelbox is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Murmelbox is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Murmelbox.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef IDC_STATIC
#define IDC_STATIC (-1)
#endif

#define IDOK                                    1000
#define IDCANCEL                                1001
#define TBM_TRANSPARENCY                        1002
#define DLG_SCRNSAVECONFIGURE                   2003
#define BTN_COLORPICK                           2005
#define EDT_BALLS                               2009
#define EDT_COLOR                               2010
#define TBM_ROTSPEED                            2011
#define TBM_BALLSPEED                           2012
#define TBM_DISTANCE                            2014
#define IDS_DESCRIPTION                         1

