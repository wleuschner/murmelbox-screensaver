/*    This file is part of Murmelbox.

    Murmelbox is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Murmelbox is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Murmelbox.  If not, see <http://www.gnu.org/licenses/>.
*/
#include"vector3d.h"
#include<cmath>

using namespace std;

Vector3D Normalize3D(Vector3D *a)
{
	Vector3D b;
	float len=sqrt(a->x*a->x+a->y*a->y+a->z*a->z);
	b.x=a->x/len;
	b.y=a->y/len;
	b.z=a->z/len;
	return b;
}

float DistPointPoint(Vector3D* a,Vector3D* b)
{
	return sqrt(a->x*b->x+a->y*b->y+a->z*b->z);
}

float DistPlanePoint(Vector3D* origin,Vector3D* norm,Vector3D* point)
{
	float a,b;
	a=origin->x*norm->x+origin->y*norm->y+origin->z*norm->z;
	b=point->x*norm->x+point->y*norm->y+point->z*norm->z;
	return fabs(b-a);
}

float IntersectPlaneRay(Vector3D* origin,Vector3D* norm,Vector3D* point,Vector3D* dir)
{
	float a,b;
	a=-((norm->x*point->x+norm->y*point->y+norm->z*point->z)-(norm->x*origin->x+norm->y*origin->y+norm->z*origin->z));
	b=norm->x*dir->x+norm->y*dir->y+norm->z*dir->z;
	return a/b;
}

