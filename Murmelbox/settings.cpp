/*    This file is part of Murmelbox.

    Murmelbox is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Murmelbox is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Murmelbox.  If not, see <http://www.gnu.org/licenses/>.
*/
#include"settings.h"
#include<cstdlib>
#include<windows.h>
#include<tchar.h>
using namespace std;

void writeSettings(Settings* cfg)
{
	HKEY rootKey;
	RegCreateKeyEx(HKEY_CURRENT_USER,_T("Software\\Murmelbox"),0,0,REG_OPTION_NON_VOLATILE,KEY_ALL_ACCESS,0,&rootKey,0);
	RegSetValueEx(rootKey,_T("transparency"),0,REG_BINARY,(BYTE*)&(cfg->trans),sizeof(float));
	RegSetValueEx(rootKey,_T("distance"),0,REG_BINARY,(BYTE*)&(cfg->dist),sizeof(float));
	RegSetValueEx(rootKey,_T("rotspeed"),0,REG_BINARY,(BYTE*)&(cfg->rotspeed),sizeof(float));
	RegSetValueEx(rootKey,_T("ballspeed"),0,REG_BINARY,(BYTE*)&(cfg->ballspeed),sizeof(float));
	RegSetValueEx(rootKey,_T("numballs"),0,REG_DWORD,(BYTE*)&(cfg->numBalls),sizeof(DWORD));
	RegSetValueEx(rootKey,_T("bgcolor"),0,REG_DWORD,(BYTE*)&(cfg->clearcolor),sizeof(DWORD));
	RegCloseKey(rootKey);
}

Settings* readSettings()
{
	HKEY rootKey;
	DWORD floatSize=sizeof(float);
	DWORD dwordSize=sizeof(DWORD);
	Settings *set=(Settings*)malloc(sizeof(Settings));

	//Load Registry Settings
	if(RegOpenKeyEx(HKEY_CURRENT_USER,_T("Software\\Murmelbox"),REG_OPTION_NON_VOLATILE,KEY_ALL_ACCESS,&rootKey)==ERROR_SUCCESS)
	{
		RegQueryValueEx(rootKey,_T("transparency"),0,0,(BYTE*)&(set->trans),&floatSize);
		RegQueryValueEx(rootKey,_T("distance"),0,0,(BYTE*)&(set->dist),&floatSize);
		RegQueryValueEx(rootKey,_T("rotspeed"),0,0,(BYTE*)&(set->rotspeed),&floatSize);
		RegQueryValueEx(rootKey,_T("ballspeed"),0,0,(BYTE*)&(set->ballspeed),&floatSize);
		RegQueryValueEx(rootKey,_T("numballs"),0,0,(BYTE*)&(set->numBalls),&dwordSize);
		RegQueryValueEx(rootKey,_T("bgcolor"),0,0,(BYTE*)&(set->clearcolor),&dwordSize);
		RegCloseKey(rootKey);
	}
	//Load Default Settings
	else
	{
		set->dist=4.0f;
		set->rotspeed=1.0f;
		set->ballspeed=1.0f;
		set->trans=1.0f;
		set->numBalls=20;
		set->clearcolor=0;
	}
	return set;
}

