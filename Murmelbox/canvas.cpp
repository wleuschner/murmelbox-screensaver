/*    This file is part of Murmelbox.

    Murmelbox is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Murmelbox is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Murmelbox.  If not, see <http://www.gnu.org/licenses/>.
*/
#include"canvas.h"
#include<windows.h>
#include<windowsx.h>
#include<GL/gl.h>
#include<GL/glu.h>
#include<GL/glext.h>

static HDC hDC;
static HGLRC hRC;
static unsigned int texid;

void SwapBuffer()
{
	SwapBuffers(hDC);
}

void InitGL(HWND hWnd)
{
    int format;
	RECT rect;
    PIXELFORMATDESCRIPTOR pfd;
	GetClientRect(hWnd,&rect);
    ZeroMemory(&pfd,sizeof(pfd));
    pfd.nSize=sizeof(pfd);
    pfd.nVersion=1;
    pfd.dwFlags=PFD_SUPPORT_OPENGL|PFD_DOUBLEBUFFER;
    pfd.iPixelType=PFD_TYPE_RGBA;
    pfd.cColorBits=24;
	pfd.cDepthBits=8;
    hDC=GetDC(hWnd);
    format=ChoosePixelFormat(hDC,&pfd);
    SetPixelFormat(hDC,format,&pfd);
    hRC=wglCreateContext(hDC);
	wglMakeCurrent(hDC,hRC);

	glDepthFunc(GL_LEQUAL);
    glEnableClientState(GL_VERTEX_ARRAY);
	glEnableClientState(GL_NORMAL_ARRAY);
	glEnableClientState(GL_TEXTURE_COORD_ARRAY);
    glEnable(GL_LIGHTING);
    glEnable(GL_DEPTH_TEST);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
	gluPerspective(45.0f,(float)rect.right/(float)rect.bottom,0.1f,100.0f);
	glViewport(0,0,rect.right,rect.bottom);
    glMatrixMode(GL_MODELVIEW);

	glEnable(GL_TEXTURE_2D);
	glBlendFunc(GL_SRC_ALPHA,GL_ONE);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT,GL_NICEST);
}

void RGBtoFloat(int color,float* rf,float* gf,float* bf)
{
	int r,g,b;
	r=color&0xff;
	g=(color>>8)&0xff;
	b=(color>>16)&0xff;
	*rf=r/255.0f;
	*gf=g/255.0f;
	*bf=b/255.0f;
}


int GetDesktopTexture()
{
	DWORD bmpSize;
	char* buffer=0;
	BITMAPINFOHEADER bi;
	BITMAPINFO info;
	HDC hDesktopDC,hBitmapDC;
	HBITMAP hBitmap;
	int iWidth,iHeight;
	hDesktopDC=GetWindowDC(HWND_DESKTOP);
	if(hDesktopDC)
	{
		iWidth=GetSystemMetrics(SM_CXSCREEN);
		iHeight=GetSystemMetrics(SM_CYSCREEN);
		hBitmap=CreateCompatibleBitmap(hDesktopDC,iWidth,iHeight);
		hBitmapDC=CreateCompatibleDC(hDesktopDC);
		SelectObject(hBitmapDC,hBitmap);
		BitBlt(hBitmapDC,0,0,iWidth,iHeight,hDesktopDC,0,0,SRCCOPY);
		bi.biSize=sizeof(BITMAPINFOHEADER);
		bi.biWidth=iWidth;
		bi.biHeight=iHeight;
		bi.biPlanes=1;
		bi.biBitCount=32;
		bi.biCompression=BI_RGB;
		bi.biSizeImage=0;
		bi.biXPelsPerMeter=0;
		bi.biYPelsPerMeter=0;
		bi.biClrUsed=0;
		bi.biClrImportant=0;
		info.bmiHeader=bi;
		info.bmiColors->rgbBlue=0;
		info.bmiColors->rgbGreen=0;
		info.bmiColors->rgbRed=0;
		info.bmiColors->rgbReserved=0;
		bmpSize=((iWidth*bi.biBitCount+31)/32)*4*iHeight;
		buffer=(char*)malloc(bmpSize);
		GetDIBits(hBitmapDC,hBitmap,0,(UINT)iHeight,buffer,&info,DIB_RGB_COLORS);
		DeleteDC(hBitmapDC);
		ReleaseDC(HWND_DESKTOP,hDesktopDC);
		glGenTextures(1,&texid);
		glBindTexture(GL_TEXTURE_2D,texid);
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR_MIPMAP_LINEAR);
		gluBuild2DMipmaps(GL_TEXTURE_2D,3,iWidth,iHeight,GL_BGRA_EXT,GL_UNSIGNED_BYTE,buffer);
		return TRUE;
	}
	return FALSE;
}

