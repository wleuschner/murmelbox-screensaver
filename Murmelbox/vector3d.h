/*    This file is part of Murmelbox.

    Murmelbox is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Murmelbox is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Murmelbox.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once

typedef struct Vector3D
{
	float x;
	float y;
	float z;
} _VECTOR3D;

Vector3D Normalize3D(Vector3D* a);
float DistPlanePoint(Vector3D* origin,Vector3D* norm,Vector3D* point);
float DistPointPoint(Vector3D* a,Vector3D* b);
float IntersectPlaneRay(Vector3D* origin,Vector3D* norm,Vector3D* point,Vector3D* dir);

