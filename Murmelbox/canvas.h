/*    This file is part of Murmelbox.

    Murmelbox is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Murmelbox is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Murmelbox.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once
#include<windows.h>

void SwapBuffer();
void InitGL(HWND hWnd);
int GetDesktopTexture();
void RGBtoFloat(int color,float* rf,float* gf,float* bf);

