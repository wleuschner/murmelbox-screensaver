/*    This file is part of Murmelbox.

    Murmelbox is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Murmelbox is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Murmelbox.  If not, see <http://www.gnu.org/licenses/>.
*/

#include"box.h"

float box_verts[]={-1.0f, -1.0f,  1.0f,
				    1.0f, -1.0f,  1.0f,
					1.0f,  1.0f,  1.0f,
				   -1.0f,  1.0f,  1.0f,

				   -1.0f, -1.0f, -1.0f,
				   -1.0f,  1.0f, -1.0f,
					1.0f,  1.0f, -1.0f,
					1.0f, -1.0f, -1.0f,

					1.0f, -1.0f, -1.0f,
					1.0f,  1.0f, -1.0f,
					1.0f,  1.0f,  1.0f,
					1.0f, -1.0f,  1.0f,

					-1.0f, -1.0f, -1.0f,
					-1.0f, -1.0f,  1.0f,
					-1.0f,  1.0f,  1.0f,
					-1.0f,  1.0f, -1.0f,

					-1.0f,  1.0f, -1.0f,
					-1.0f,  1.0f,  1.0f,
					1.0f,  1.0f,  1.0f,
					1.0f,  1.0f, -1.0f,

					-1.0f, -1.0f, -1.0f,
					1.0f, -1.0f, -1.0f,
					1.0f, -1.0f,  1.0f,
					-1.0f, -1.0f,  1.0f,


};

float box_normals[]={0.0f, 0.0f, 1.0f,
					 0.0f, 0.0f, 1.0f,
					 0.0f, 0.0f, 1.0f,
					 0.0f, 0.0f, 1.0f,

					 0.0f, 0.0f,-1.0f,
					 0.0f, 0.0f,-1.0f,
					 0.0f, 0.0f,-1.0f,
					 0.0f, 0.0f,-1.0f,

					 1.0f, 0.0f, 0.0f,
					 1.0f, 0.0f, 0.0f,
					 1.0f, 0.0f, 0.0f,
					 1.0f, 0.0f, 0.0f,

					 -1.0f, 0.0f, 0.0f,
					 -1.0f, 0.0f, 0.0f,
					 -1.0f, 0.0f, 0.0f,
					 -1.0f, 0.0f, 0.0f,

					 0.0f, 1.0f, 0.0f,
					 0.0f, 1.0f, 0.0f,
					 0.0f, 1.0f, 0.0f,
					 0.0f, 1.0f, 0.0f,

					 0.0f,-1.0f, 0.0f,
					 0.0f,-1.0f, 0.0f,
					 0.0f,-1.0f, 0.0f,
					 0.0f,-1.0f, 0.0f,
};

extern float face_norms[]={
					 0.0f, 0.0f, 1.0f,

					 0.0f, 0.0f,-1.0f,

					 1.0f, 0.0f, 0.0f,

					 -1.0f, 0.0f, 0.0f,

					 0.0f, 1.0f, 0.0f,

					 0.0f,-1.0f, 0.0f,
};

float box_texcoords[]={0.0f, 0.0f,
					   1.0f, 0.0f,
					   1.0f, 1.0f,
					   0.0f, 1.0f,

					   1.0f, 0.0f,
					   1.0f, 1.0f,
					   0.0f, 1.0f,
					   0.0f, 0.0f,

					   1.0f, 0.0f,
					   1.0f, 1.0f,
					   0.0f, 1.0f,
					   0.0f, 0.0f,

					   0.0f, 0.0f,
					   1.0f, 0.0f,
					   1.0f, 1.0f,
					   0.0f, 1.0f,

					   0.0f, 1.0f,
					   0.0f, 0.0f,
					   1.0f, 0.0f,
					   1.0f, 1.0f,

					   1.0f, 1.0f,
					   0.0f, 1.0f,
					   0.0f, 0.0f,
					   1.0f, 0.0f
};

