/*    This file is part of Murmelbox.

    Murmelbox is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Murmelbox is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Murmelbox.  If not, see <http://www.gnu.org/licenses/>.
*/
#include"sphere.h"
#include<cmath>
#include<cstdlib>
using namespace std;
#define PI2 2*3.14159265f

void GenSphere(float radius,int slices,int stacks,float** verts,float** normals)
{
    int x,y;
    float incx=PI2/slices;
    float incy=PI2/stacks;

    float phi;
    float theta;

	float* beginverts;
	float* beginnormals;

    *verts=(float*)malloc(sizeof(float)*slices*stacks*4*3);
    *normals=(float*)malloc(sizeof(float)*slices*stacks*4*3);

	beginverts=*verts;
	beginnormals=*normals;

    for(phi=0.0f,x=0;x<slices;x++,phi+=incx)
    {
        for(theta=0.0f,y=0;y<stacks;y++,theta+=incy)
        {
            *beginnormals=cosf(phi)*sinf(theta);
	        beginnormals++;
            *beginnormals=cosf(theta);
            beginnormals++;
            *beginnormals=sinf(theta)*sinf(phi);
            beginnormals++;
            *beginverts=cosf(phi)*sinf(theta)*radius;
            beginverts++;
            *beginverts=cosf(theta)*radius;
            beginverts++;
            *beginverts=sinf(theta)*sinf(phi)*radius;
            beginverts++;

            *beginnormals=cosf(phi)*sinf(theta+incx);
            beginnormals++;
            *beginnormals=cosf(theta+incx);
            beginnormals++;
            *beginnormals=sinf(theta+incx)*sinf(phi);
            beginnormals++;
            *beginverts=cosf(phi)*sinf(theta+incx)*radius;
            beginverts++;
            *beginverts=cosf(theta+incx)*radius;
            beginverts++;
            *beginverts=sinf(theta+incx)*sinf(phi)*radius;
            beginverts++;

            *beginnormals=cosf(phi+incx)*sinf(theta);
            beginnormals++;
            *beginnormals=cosf(theta);
            beginnormals++;
            *beginnormals=sinf(theta)*sinf(phi+incx);
            beginnormals++;
            *beginverts=cosf(phi+incx)*sinf(theta)*radius;
            beginverts++;
            *beginverts=cosf(theta)*radius;
            beginverts++;
            *beginverts=sinf(theta)*sinf(phi+incx)*radius;
            beginverts++;

            *beginnormals=cosf(phi+incx)*sinf(theta+incx);
            beginnormals++;
            *beginnormals=cosf(theta+incx);
            beginnormals++;
            *beginnormals=sinf(theta+incx)*sinf(phi+incx);
            beginnormals++;
            *beginverts=cosf(phi+incx)*sinf(theta+incx)*radius;
            beginverts++;
            *beginverts=cosf(theta+incx)*radius;
            beginverts++;
            *beginverts=sinf(theta+incx)*sinf(phi+incx)*radius;
            beginverts++;
        }
    }
}

