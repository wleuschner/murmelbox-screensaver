/*    This file is part of Murmelbox.

    Murmelbox is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Murmelbox is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Murmelbox.  If not, see <http://www.gnu.org/licenses/>.
*/
#include<windows.h>
#include<commctrl.h>
#include<scrnsave.h>
#include<GL/gl.h>
#include<ctime>
#include<tchar.h>
#include"canvas.h"
#include"saver.h"
#include"sphere.h"
#include"settings.h"
#include"resource.h"
#define EVENTTIMER 1

using namespace std;

LRESULT WINAPI ScreenSaverProc(HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam)
{
	static Settings *cfg;
	static DEVMODE monitcfg;
	static float r,g,b;
	switch(msg)
	{
	case WM_CREATE:
		srand(time(0));
		EnumDisplaySettings(0,ENUM_CURRENT_SETTINGS,&monitcfg);
		cfg=readSettings();
		cfg->refreshRate=monitcfg.dmDisplayFrequency;
		InitGL(hWnd);
		RGBtoFloat(cfg->clearcolor,&r,&g,&b);
		glClearColor(r,g,b,1.0f);
		GenCubeMaterial(1.0f-cfg->trans);
		SetupGeometry();
		GetDesktopTexture();
		SetupBalls(cfg->numBalls);
		SetupLight();
		SetTimer(hWnd,EVENTTIMER,1000/cfg->refreshRate,0);
		return 0;
	case WM_TIMER:
		Update(cfg);
		Draw(cfg);
		return 0;
	case WM_DESTROY:
		free(cfg);
		break;
	default:
		return DefScreenSaverProc(hWnd,msg,wParam,lParam);
	}
	return 0;
}

BOOL WINAPI ScreenSaverConfigureDialog(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	static Settings *cfg;
	static TCHAR buffer[128];
	static DWORD customcolors[16];
	switch(message)
	{
		case WM_INITDIALOG:
			InitCommonControls();
			cfg=readSettings();
			_itot(cfg->numBalls,buffer,10);
			SetDlgItemText(hDlg,EDT_BALLS,buffer);
			_itot(cfg->clearcolor,buffer,16);
			SetDlgItemText(hDlg,EDT_COLOR,buffer);
			SendDlgItemMessage(hDlg,TBM_TRANSPARENCY,TBM_SETRANGE,TRUE,MAKELONG(0,16382));
			SendDlgItemMessage(hDlg,TBM_TRANSPARENCY,TBM_SETPOS,TRUE,cfg->trans*16382);
			SendDlgItemMessage(hDlg,TBM_ROTSPEED,TBM_SETRANGE,TRUE,MAKELONG(0,16382));
			SendDlgItemMessage(hDlg,TBM_ROTSPEED,TBM_SETPOS,TRUE,cfg->rotspeed/10.0f*16382);
			SendDlgItemMessage(hDlg,TBM_BALLSPEED,TBM_SETRANGE,TRUE,MAKELONG(0,16382));
			SendDlgItemMessage(hDlg,TBM_BALLSPEED,TBM_SETPOS,TRUE,cfg->ballspeed/10.0f*16382);
			SendDlgItemMessage(hDlg,TBM_DISTANCE,TBM_SETRANGE,TRUE,MAKELONG(0,16382));
			SendDlgItemMessage(hDlg,TBM_DISTANCE,TBM_SETPOS,TRUE,cfg->dist/10.0f*16382);
			return TRUE;
		case WM_COMMAND:
		{
			switch(LOWORD(wParam))
			{
				case BTN_COLORPICK:
				{
					CHOOSECOLOR ccw;
					ZeroMemory(&ccw,sizeof(CHOOSECOLOR));
					ccw.hwndOwner=hDlg;
					ccw.lStructSize=sizeof(CHOOSECOLOR);
					ccw.Flags=CC_RGBINIT|CC_FULLOPEN;
					ccw.lpCustColors=customcolors;
					GetDlgItemText(hDlg,EDT_COLOR,buffer,128);
					ccw.rgbResult=_ttoi(buffer);
					if(ChooseColor(&ccw)==TRUE)
					{
						cfg->clearcolor=ccw.rgbResult;
						_itot(ccw.rgbResult,buffer,16);
						SetDlgItemText(hDlg,EDT_COLOR,buffer);
					}
					break;
				}
				case IDOK:
					GetDlgItemText(hDlg,EDT_BALLS,buffer,128);
					cfg->numBalls=_ttoi(buffer);
					cfg->trans=SendDlgItemMessage(hDlg,TBM_TRANSPARENCY,TBM_GETPOS,0,0)*1.0f/16382;
					cfg->rotspeed=SendDlgItemMessage(hDlg,TBM_ROTSPEED,TBM_GETPOS,0,0)*10.0f/16382;
					cfg->ballspeed=SendDlgItemMessage(hDlg,TBM_BALLSPEED,TBM_GETPOS,0,0)*10.0f/16382;
					cfg->dist=SendDlgItemMessage(hDlg,TBM_DISTANCE,TBM_GETPOS,0,0)*10.0f/16382;
					writeSettings(cfg);
				case IDCANCEL:
					PostQuitMessage(0);
			}
		}
		break;
		case WM_CLOSE:
			free(cfg);
			PostQuitMessage(0);
			return TRUE;
	}
	return FALSE;
}

BOOL WINAPI RegisterDialogClasses(HANDLE hInst)
{
    return TRUE;
}

